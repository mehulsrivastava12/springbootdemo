#docker run -d   --name mysql   -e MYSQL_ROOT_PASSWORD=root   -e MYSQL_DATABASE=onlearning   mysql:5
FROM openjdk:8
WORKDIR /app
COPY . .
RUN apt update && apt install maven -y
RUN mvn clean install -DskipTests
EXPOSE 8080
CMD ["java","-jar","target/demo-0.0.1-SNAPSHOT.jar"]
