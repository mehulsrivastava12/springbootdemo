<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>My Videos</title>
</head>
<body>
<jsp:include page="instructorNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div
        class="container mt-3 card mt-5 shadow-lg p-3 mb-5 bg-white rounded">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center mt-5 mb-5 card bg-light"
                style="width: 700px; margin-left: 200px;">My Video</h3>
            <form <%--action="${pageContext.request.contextPath }/addvideoform/${cid}"--%> id="myVideos" method="post">
                <table class="table table-hover justify-content-center text-center">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Video Title</th>
                        <th scope="col">Video Description</th>
                        <th scope="col">Video Link</th>
                        <th scope="col">Course Documents</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${courseVideos }" var="p">
                        <tr>
                            <th>${p.title }</th>
                            <td>${p.description }</td>
                            <td><a href="${p.link }" target="_blank">Click Here</a></td>
                            <td>
                                <c:forEach items="${p.courseDocuments}" var="courseDocument">
                                    ${courseDocument.fileName}
                                    <a href="/v1/downloadFile/${courseDocument.fileName}/${courseDocument.courseVideo.videoId}"><img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAB8CAMAAACcwCSMAAAAWlBMVEX///8AAACEhIStra3x8fHIyMhfX19ycnL4+PiKioro6OgJCQkqKipISEh2dnalpaWenp6/v79nZ2cvLy86OjojIyNYWFg1NTUSEhLS0tK4uLiWlpZOTk7Z2dl80dIPAAAB20lEQVRoge3a63KCMBAF4FWRchMUL3h9/9eszlRLYiib7JJOp+f8Vj6F2WRJQhSQdTWzUq1DrhOS1KYfSePYG5c9m22i4Lkbz6Pgczc+Bw4cOHDgwIEDBw4cOHDgwIEDBw4cOHDgwIH/ETziTkPWtKmZrRvfWh9rm0xs79wUJzupPnCPeRE+iayT4J3sr2d7Cb4X3vdWgrcymwoJXgjxoc1LThQ2OJtQu5HbwdWmNOLVIXatYxMd/O2Dlh0w1AiHFyPF0c8+iousn1PpY5cnTZvo7IOfdW2iBd9eaNtEF6590bfZ5a5W4GauHPs6jU3Z28Gg91SKBW6mGC24UrXAzdxG9PI2nT06u098ROnHcp+gwM0MvCw9EuGFabCllLaLrDiPw8U6EJesXPYqiYJT4XiT2E9Y4GZu73hwgSftRz/t+IWWtr0c/8EW8nxKifeV7Gae0aLbv1eAm808p0XXxPuzO2sGV8WzV7mnrFlUFafkq5nveAWuiz+/xhxclHFK6qqquQObNn5/8PymSR/3CHDgwIEDB/4r+HwxQewXzCE8SoADB/5/cNmeZVi+l668thF00tuMcK4vTZlVb8opGIvZmjHXroqAfbvwHOx1s2XeeW1fhabs8le/8Al7jiDGuJqKTwAAAABJRU5ErkJggg=="
                                            style="width: 15px; float:right;"/></a><br><br>
                                </c:forEach>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="text-center mb-5 mt-5">
                    <button type="button" id="addVideos" class="btn btn-outline-success">ADD MORE VIDEO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function addVideos() {
        var token = localStorage.getItem('token')
        var form = $("#myVideos");
        var formData = form.serialize();
        let url = form.attr("action")
        var headers = {
            "Authorization": "Bearer " + token
        };

        $.ajax({
            url: "${pageContext.request.contextPath}/v1/addvideoform/${cid}",
            type: "POST",
            data: formData,
            headers: headers,
            dataType: "html",
            success: function (data) {
                document.open();
                document.write(data);
                document.close();
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }

    $("#addVideos").on("click", addVideos);
</script>
</body>
</html>
