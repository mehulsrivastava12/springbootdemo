<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<!doctype html>
<html lang="en">
<%@taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <%@ include file="./base.jsp" %>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<style>
    body {
        background-image: url('https://img.freepik.com/premium-photo/abstract-education-sketch_670147-41734.jpg?w=740');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        background-attachment: fixed;
        height: 100%;
        margin: 0;
    }
</style>
<body>
<div class="container mt-5">
    <div class="card" style="width: 600px; margin-left: 250px;">
        <h3 class="text-center mt-5">Log In Using OTP</h3>
        <form action="sendOTP" method="post">
            <div class="form-group mt-5 ml-5 mr-5">
                <label for="phoneNumber">Phone Number</label> <input
                    type="text" class="form-control" id="phoneNumber"
                    aria-describedby="phoneNumber" placeholder="+91123456789"
                    name="phoneNumber" required>
            </div>

            <div class="text-center">
                <p class="text-danger">${msg }</p>
            </div>
            <div class="container text-center mb-3">
                <button type="submit" class="btn btn-success">Send OTP</button>
            </div>
            <div class="text-center mb-2">
                <span>Don't Have A Account? <a href="signup">Sign Up</a></span>
            </div>

            <div class="text-center mb-2">
					<span>Login As Instructor !! <a href="loginInstructor">LogIn
							As Instructor</a></span>
            </div>
            <div></div>
        </form>
    </div>
</div>
</body>
<%--<script>--%>
<%--    localStorage.removeItem('load');--%>
<%--    sessionStorage.removeItem('token');--%>
<%--</script>--%>
</html>
