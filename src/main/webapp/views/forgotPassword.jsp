<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<!doctype html>
<html lang="en">
<%@taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <%@ include file="./base.jsp" %>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<style>
    body {
        background-image: url('https://img.freepik.com/premium-photo/abstract-education-sketch_670147-41734.jpg?w=740');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        background-attachment: fixed;
        height: 100%;
        margin: 0;
    }
</style>
<body>
<div class="container mt-5">
    <div class="card" style="width: 600px; margin-left: 250px;">
        <h3 class="text-center mt-5">Forgot Password</h3>
        <form action="forgotPasswordOTP" method="post">
            <div class="form-group mt-5 ml-5 mr-5">
                <label for="email">Email</label> <input
                    type="text" class="form-control" id="email"
                    aria-describedby="email" placeholder="Enter your Email"
                    name="email" required>
            </div>

            <div class="text-center">
                <p class="text-danger">${msg }</p>
            </div>
            <div class="container text-center mb-3">
                <button type="submit" class="btn btn-success">Send OTP</button>
            </div>
            <div class="text-center mb-2">
                <a href="${pageContext.request.contextPath}/v1/">Go Back</a>
            </div>
            <div></div>
        </form>
    </div>
</div>
</body>
</html>
