<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>My Courses</title>
    <style>
        .link-like-text {
            color: #007bff;
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
</head>
<body>
<jsp:include page="userNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div class="container mt-3">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center mt-5 mb-5 card alert alert-info"
                style="width: 700px; margin-left: 200px;">My Courses</h3>
            <table class="table table-hover justify-content-center text-center">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Course Title</th>
                    <th scope="col">Instructor Name</th>
                    <th scope="col">Date Enrolled</th>
                    <th scope="col">Go To Course</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${myCourse }" var="p">
                    <tr>
                        <td scope="row">${p.title}</td>
                        <td>${p.firstName } ${p.lastName }</td>
                        <td><fmt:formatDate value="${p.date}" pattern="dd/MM/yyyy"/></td>
                        <td class="myCourses link-like-text" data-id="${p.courseId}">Click Here</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var token = localStorage.getItem('token');
        var headers = {
            "Authorization": "Bearer " + token,
        };
        $(".myCourses").on("click", function () {
            var courseId = $(this).data('id');
            $.ajax({
                url: `${pageContext.request.contextPath }/v1/usercoursevideo/` + courseId + "/" + ${id},
                contentType: 'application/json',
                type: 'GET',
                dataType: "html",
                headers: headers,
                success: function (data) {
                    document.open();
                    document.write(data);
                    document.close();
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
</script>
</body>
</html>
