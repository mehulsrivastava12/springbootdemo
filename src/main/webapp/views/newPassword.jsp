<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<!doctype html>
<html lang="en">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <%@ include file="./base.jsp" %>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body class="bg-info">
<div class="container mt-5">
    <div class="card" style="width: 600px; margin-left: 250px;">
        <h3 class="text-center mt-5">Enter New Password</h3>
        <form action="changePassword" method="post">
            <div class="form-group ">
                <input type=hidden id="email" placeholder="Enter User Id"
                       name="email" value="${email }" readOnly>
            </div>
            <div class="text-center">
                <p class="text-danger">${msg }</p>
            </div>
            <div class="form-group mt-5 ml-5 mr-5">
                <label for="newPassword">New Password</label> <input
                    type="password" class="form-control" id="newPassword"
                    aria-describedby="newPassword" placeholder="Enter New Password"
                    name="newPassword" required>
            </div>
            <div class="container text-center mb-3">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            <div class="text-center mb-2">
                <span>Don't Have A Account? <a href="signup">Sign Up</a></span>
            </div>

            <div class="text-center mb-2">
					<span>Login As Instructor !! <a href="loginInstructor">LogIn
							As Instructor</a></span>
            </div>
            <div></div>
        </form>
    </div>
</div>
</body>
</html>
