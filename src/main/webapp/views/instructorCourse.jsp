<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Instructor Course</title>
</head>
<style>
    .link-like-text {
        color: #007bff;
        cursor: pointer;
        text-decoration: underline;
    }
</style>
<body class="bg-light">
<jsp:include page="instructorNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<form method="post">
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center mt-5 mb-5 card alert alert-primary"
                    style="width: 500px; margin-left: 300px">Instructor Course</h1>
                <table
                        class="table table-hover justify-content-center text-center ml-3 mr-3">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Course Title</th>
                        <th scope="col">Course Description</th>
                        <th scope="col">Course Price</th>
                        <th scope="col">Add Video</th>
                        <th scope="col">Show Tutorial</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${instructorCourse }" var="p">
                        <tr>
                            <td>${p.title }</td>
                            <td>${p.description }</td>
                            <td>${p.price }</td>
                            <td class="clickable link-like-text addCourse" data-id="${p.cid}">Add Course Video</td>
                            <td class="clickHere link-like-text tutorials" data-id="${p.cid}">Click Here</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        var token = localStorage.getItem('token');
        var headers = {
            "Authorization": "Bearer " + token,
        };
        $(".addCourse").on("click", function () {
            var courseId = $(this).data('id');
            $.ajax({
                url: `${pageContext.request.contextPath}/v1/addvideoform/` + courseId,
                contentType: 'application/json',
                type: 'GET',
                dataType: "html",
                headers: headers,
                success: function (data) {
                    document.open();
                    document.write(data);
                    document.close();
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
        });
        $(".tutorials").on("click", function () {
            var courseId = $(this).data('id');
            console.log("________", courseId)
            $.ajax({
                url: `${pageContext.request.contextPath}/v1/mycoursevideo/` + courseId,
                contentType: 'application/json',
                type: 'GET',
                dataType: "html",
                headers: headers,
                success: function (data) {
                    document.open();
                    document.write(data);
                    document.close();
                },
                error: function (xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
</script>
</body>
</html>
