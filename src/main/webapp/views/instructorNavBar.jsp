<head>
    <script type="text/javascript" src="/script.js"></script>
</head>

<style>
    .clickable {
        cursor: pointer;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-secondary">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul>
            <img style="height: 100px; width: 100px; border-radius: 20px;"
                 class="card-img-top mt-2"
                 src="https://st2.depositphotos.com/3096625/7016/v/950/depositphotos_70164723-stock-illustration-online-language-learning-logo.jpg"
                 alt="Card image cap"/>
        </ul>
        <form action="search/" method="post" class="ml-5 mt-3">
            <input style="border-radius: 10px; width: 500px;" type="text"
                   placeholder="Search Here" name="instructor">
            <button class="tn btn-outline-info text-white my-2 my-sm-0"
                    style="border-radius: 20px;">Search
            </button>
        </form>
        <ul class="navbar-nav mr-auto ml-3">
            <li class="nav-item active ml-5"><a
                    class="nav-link text-white font-weight-bold clickable"
                    onclick="addCourse(${id})">Add Course</a></li>
            <li class="nav-item active ml-5"><a
                    class="nav-link text-white font-weight-bold clickable"
                    onclick="instructorCourses(${id})">My Courses <span class="sr-only">(current)</span></a>
            </li>
            <li style="position: relative;display: inline-block;">
                <a class="dropdown-toggle nav-item active ml-5 text-white font-weight-bold" style="margin-top: 8px"
                   type="button" id="instructorDropdown" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                </a>
                <div class="dropdown-menu" style="margin-left: 40px" aria-labelledby="instructorDropdown">
                    <a class="dropdown-item clickable" onclick="instructorProfile(${id})">My Profile</a>
                    <a class="dropdown-item clickable" data-toggle="modal"
                       data-target="#exampleModal">Log Out<span class="sr-only">(current)</span></a>
                </div>
            </li>
            <div class="modal fade" id="exampleModal" tabindex="-1"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Log Out</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">Are You Sure You Want To Log Out</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Cancel
                            </button>
                            <form action="${pageContext.request.contextPath }/v1/"
                                  method="post">
                                <button type="submit" class="btn btn-outline-danger" onclick="logout()">Log
                                    Out
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </ul>
    </div>
</nav>

<script>
    $(document).ready(function () {
        $.ajax({
            url: "/v1/instructor/${id}",
            type: "GET",
            dataType: 'json',
            success: function (data) {
                $('#instructorDropdown').text(data.firstName + " " + data.lastName);
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    })
</script>

