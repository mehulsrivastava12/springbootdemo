<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Video</title>
</head>
<body class="bg-light">
<jsp:include page="instructorNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div class="card mt-5 shadow p-3 mb-5 bg-white rounded" style="width:500px;margin-left:450px">
    <h3 class="text-center mt-5">Add Video To Course</h3>
    <div>
        <form
                action="${pageContext.request.contextPath }/v1/addvideo/${cid }"
                method="post" enctype="multipart/form-data" class="bg-white" id="newVideos">
            <div>
                <input type="hidden"
                       class="form-control" id="course" placeholder="Enter Course Id"
                       name="course" value="${cid }" required>
            </div>

            <div>
                <label for="title" class="mt-5">Video Title</label> <input type="text"
                                                                           class="form-control" id="title"
                                                                           placeholder="Enter Video Title"
                                                                           name="title" required>
            </div>

            <div>
                <label for="description" class="mt-3">Course Description</label> <input
                    type="text" id="description" class="form-control"
                    placeholder="Enter Course Description" name="description" required>
            </div>

            <div>
                <label for="link" class="mt-3">Tutorial Link</label> <input
                    type="text" id="link" class="form-control" placeholder="Enter The Tutorial Link" name="link"
                    required>
            </div>

            <div>
                <label for="link" class="mt-3">Course Documents</label><br>
                <input type="file" name="files" id="filesToUpload" multiple>
            </div>


            <div class="container text-center mb-3 mt-5">
                <button type="button" class="btn btn-outline-success" id="addVideo">ADD VIDEO</button>
            </div>
        </form>
    </div>
</div>

<script>
    function addVideo() {
        var token = localStorage.getItem('token')
        var form = $("#newVideos")[0];
        var formData = new FormData(form);
        var headers = {
            "Authorization": "Bearer " + token
        };

        $.ajax({
            url: "/v1/addvideo/${cid}",
            type: "POST",
            data: formData,
            processData: false, // Prevent jQuery from processing the data
            contentType: false, // Prevent jQuery from setting content type
            headers: headers,
            dataType: "html",
            success: function (data) {
                document.open();
                document.write(data);
                document.close();
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }

    $('#addVideo').on('click', addVideo)
</script>
</body>
</html>
