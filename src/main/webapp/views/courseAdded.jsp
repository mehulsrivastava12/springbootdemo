<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Course Added</title>
</head>
<body>
<jsp:include page="instructorNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div class="alert alert-success" role="alert">
    <h4 class="alert-heading">Well done!</h4>
    <p>Successfully Added</p>
    <form
            action="${pageContext.request.contextPath }/instructorcourse/${id}" id="allEnrolledCourses">
        <button type="button" id="goMyCourse" class="btn btn-outline-secondary">Go To My Course</button>
    </form>
</div>

<script>
    function goToMyCourse() {
        var token = localStorage.getItem('token')
        var form = $("#allEnrolledCourses");
        var formData = form.serialize();
        let url = form.attr("action")
        // await apiCall(url,'POST',formData)
        var headers = {
            "Authorization": "Bearer " + token
        };

        $.ajax({
            url: "/v1/" + url,
            type: "POST",
            data: formData,
            headers: headers,
            dataType: "html",
            success: function (data) {
                document.open();
                document.write(data);
                document.close();
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }

    $("#goMyCourse").on("click", goToMyCourse);
</script>
</body>
</html>
