<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body class="bg-light">
<jsp:include page="userNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div class="card mt-5 shadow-lg p-3 mb-5 bg-white rounded" style="width:500px;margin-left:450px">
    <h1 class="mt-5 mb-5 text-center card bg-light">Update Your Details</h1>
    <form action="${pageContext.request.contextPath }/updateDetails/${id}"
          method="post" id="updatedUser">
        <input type="hidden" value="${user.uid}" name="uid" readOnly/>
        <div>
            <label for="firstName" class="mb-3">First Name</label><input class="form-control" type="text"
                                                                         id="firstName" name="firstName"
                                                                         placeholder="Enter Your First Name"
                                                                         value="${user.firstName }">
        </div>
        <div>
            <label for="lastName" class="mt-3">Last Name</label><input class="form-control" type="text"
                                                                       id="lastName" name="lastName"
                                                                       placeholder="Enter Your Last Name"
                                                                       value="${user.lastName }">
        </div>
        <div>
            <label for="dob" class="mt-3">Date Of Birth</label><input class="form-control" type="text" id="dob"
                                                                      name="dob" placeholder="Enter Your Date Of Birth"
                                                                      value="${user.dob }">
        </div>
        <div>
            <label for="email" class="mt-3">Email</label><input class="form-control" type="text" id="email"
                                                                name="email" placeholder="Enter Your Email"
                                                                value="${user.email }">
        </div>
        <div>
            <label for="phoneNumber" class="mt-3">Email</label><input class="form-control" type="tel" id="phoneNumber"
                                                                      name="phoneNumber"
                                                                      placeholder="Enter Your Phone Number"
                                                                      value="${user.phoneNumber }">
        </div>
        <div class="text-center">
            <button type="button" id="updatedDetails" onclick="updateDetails()" class="btn btn-outline-success mt-5 ">
                Submit
            </button>
        </div>
    </form>
</div>
<script>

</script>
</body>
</html>
