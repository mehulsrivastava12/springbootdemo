<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Instructor Details</title>
</head>
<body>
<jsp:include page="instructorNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<c:forEach items="${instructorCourse }" var="p">
    <div class="card d-inline-flex ml-4 mr-4 mt-5" style="width: 18rem; height:23rem;">
        <a href="#"><img class="card-img-top"
                         src="https://www.onlinecoursereport.com/wp-content/uploads/2020/06/shutterstock_1150510607-1024x512.jpg"
                         alt="Card image cap"></a>
        <div class="card-body">
            <h5 class="card-title">${p.title }</h5>
            <p class="card-text">${p.description }</p>
            <p class="card-title">Price:- &#x20B9; ${p.price }</p>
        </div>
    </div>
</c:forEach>
</body>
<script>
    window.addEventListener("load", function () {
        const authToken = "${token}"
        sessionStorage.setItem("token", authToken)
    })


    <%--    function logout() {--%>
    <%--        localStorage.removeItem('load');--%>
    <%--        sessionStorage.removeItem('token');--%>
    <%--        window.location.href = "${pageContext.request.contextPath }/loginInstructor";--%>
    <%--    }--%>

    <%--    window.addEventListener("load", function () {--%>
    <%--        const authToken= "${token}"--%>
    <%--        sessionStorage.setItem("token",authToken)--%>
    <%--        if (localStorage.getItem('load') !== ${id }) {--%>
    <%--            alert("Instructor LogIn Successfully");--%>
    <%--            localStorage.setItem('load', ${id });--%>
    <%--        }--%>
    <%--    })--%>

    <%--   --%>
</script>
</html>
