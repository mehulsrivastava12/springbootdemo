<%@page import="org.hibernate.internal.build.AllowSysOut" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page isELIgnored="false" %>
<%@ include file="./base.jsp" %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*,java.text.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <title>User Home Page</title>
</head>
<body>
<%
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String newDate = df.format(new Date());
%>
<jsp:include page="userNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<c:forEach items="${allCourses }" var="p">
    <div class="card d-inline-flex ml-4 mr-4 mt-5"
         style="width: 18rem; height: 23rem;">
        <a href="#"><img class="card-img-top"
                         src="https://www.onlinecoursereport.com/wp-content/uploads/2020/06/shutterstock_1150510607-1024x512.jpg"
                         alt="Card image cap"></a>
        <div class="card-body">
            <h5 class="card-title">${p.title }</h5>
            <p class="card-text">${p.description }</p>
            <p class="card-title">Price:- &#x20B9; ${p.price }</p>
            <div class="container" id="enrollButons" style="margin-top: 50px;">
                <button onclick="paymentStart(${p.price},${id},${p.cid},${p.instructor.getId()})"
                        class="btn btn-outline-success">Enroll Now
                </button>
            </div>
        </div>
    </div>
</c:forEach>
</body>
<script>
    window.addEventListener("load", function () {
        const authToken = "${token}"
        localStorage.setItem("token", authToken)
    })
</script>
</html>
