<html>
<head>
    <%@page isELIgnored="false" %>
    <%@ include file="./base.jsp" %>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page import="java.io.*,java.util.*, javax.servlet.*,java.text.*" %>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
</head>
<body class="bg-light">
<%
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String newDate = df.format(new Date());
%>
<jsp:include page="userNavBar.jsp">
    <jsp:param name="title" value="ONLEARNING"/>
    <jsp:param name="id" value="${id}"/>
</jsp:include>
<div class="container mt-3">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center mt-5 mb-5 card alert alert-primary"
                style="width: 500px; margin-left: 300px">All Courses</h1>
            <table
                    class="table table-hover justify-content-center text-center ml-3 mr-3">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Course Title</th>
                    <th scope="col">Course Description</th>
                    <th scope="col">Course Price</th>
                    <th scope="col">Enroll</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${allCourses }" var="p">
                    <tr>
                        <td>${p.title }</td>
                        <td>${p.description }</td>
                        <td>${p.price }</td>
                        <td>
                            <button onclick="paymentStart(${p.price},${id},${p.cid},${p.instructor.getId()})"
                                    class="btn btn-outline-primary">Enroll Now
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
