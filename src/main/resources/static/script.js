function logout() {
    // localStorage.removeItem('load');
    // sessionStorage.removeItem('token');
    document.cookie = `jwtToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
    window.location.href = "/v1/";
}

function paymentStart(amount, userId, cid, instructorId) {
    let orderId;
    $.ajax({
        url: 'create_order',
        data: JSON.stringify({amount: amount, info: 'order_request', id: userId}),
        contentType: 'application/json',
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            //invoked when success
            if (response.status === 'created') {
                // open payment form
                orderId = response.id
                let options = {
                    key: "rzp_test_4Ce0fFmWamN3Qr",
                    amount: response.amount,
                    currency: response.currency,
                    name: "Enroll for course",
                    description: "enrollment fees",
                    image: "https://st2.depositphotos.com/3096625/7016/v/950/depositphotos_70164723-stock-illustration-online-language-learning-logo.jpg",
                    order_id: response.id,
                    handler: function (response) {
                        $.ajax({
                            url: `enroll/` + userId,
                            data: JSON.stringify({courseId: cid, instructorId: instructorId}),
                            contentType: 'application/json',
                            type: 'POST',
                            dataType: 'json',
                        });
                        $.ajax({
                            url: `updatePayment`,
                            data: JSON.stringify({
                                paymentId: response.razorpay_payment_id,
                                orderId: response.razorpay_order_id,
                                status: "SUCCESS"
                            }),
                            contentType: 'application/json',
                            type: 'POST',
                            dataType: 'json',
                        });
                    },
                    "prefill": {
                        "name": "Gaurav Kumar",
                        "email": "gaurav.kumar@example.com",
                        "contact": "9000090000"
                    },
                    "notes": {
                        "address": "Razorpay Corporate Office"
                    },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                let rzp1 = new Razorpay(options);
                rzp1.on('payment.failed', function (response) {
                    $.ajax({
                        url: `updatePayment`,
                        data: JSON.stringify({paymentId: null, orderId: orderId, status: "FAILED"}),
                        contentType: 'application/json',
                        type: 'POST',
                        dataType: 'json',
                    });
                });
                rzp1.open();
            } else {

            }
        },
        error: function (error) {
            //invoked when error
            alert("Something went wrong")
        }
    })
}

async function myProfile(id) {
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/detail/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function myCourses(id) {
    // await apiCall(`mycourses/`+id,'GET',null)
    var token = localStorage.getItem('token');
    console.log("+++++++",id,token)
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/mycourses/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function allCourses(id) {
    // await apiCall(`allCourses/`+id,'GET',null)
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/allCourses/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function updateInstructorForm(id) {
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/updateInstructorForm/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

function updateInstructorDetails() {
    var token = localStorage.getItem('token')
    var form = $("#updatedInstructor");
    var formData = form.serialize();
    let url = form.attr("action")
    var headers = {
        "Authorization": "Bearer " + token
    };
    $.ajax({
        url: "/v1" + url,
        type: "POST",
        data: formData,
        headers: headers,
        dataType: "html",
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
};

async function deleteInstructor(id) {
    // await apiCall(`/deleteinstructor/`+id,'GET',null)
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/deleteinstructor/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function instructorProfile(id) {
    // await apiCall(`getinstructor/`+id,'GET',null)
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    const url = `/v1/instructorDetails/` + id
    $.ajax({
        url: url,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

// function addNewCourse() {
//     console.log("++++++++cbsbdchsbd")
//     var token = localStorage.getItem('token') // Replace with the actual token value
//     var form = $("#newCourse");
//     var formData = form.serialize();
//     let url = form.attr("action")
//     console.log("++++++++++",url)
//     var headers = {
//         "Authorization": "Bearer " + token
//     };
//
//     // await apiCall(url,'POST',formData)
//     $.ajax({
//         url: url,
//         type: "POST",
//         data: formData,
//         headers: headers,
//         dataType: "html",
//         success: function (data) {
//             document.open();
//             document.write(data);
//             document.close();
//         },
//         error: function (xhr, status, error) {
//             console.error(error);
//         }
//     });
// }

async function addCourse(id) {
    // await apiCall(`addcourse/`+id,'GET',null)
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/addcourse/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function instructorCourses(id) {
    // await apiCall(`instructorcourse/`+id,'GET',null)
    var token = localStorage.getItem('token');
    var headers = {
        "Authorization": "Bearer " + token,
    };
    $.ajax({
        url: `/v1/instructorcourse/` + id,
        contentType: 'application/json',
        type: 'GET',
        dataType: "html",
        headers: headers,
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

async function updateDetails() {
    var token = localStorage.getItem('token') // Replace with the actual token value
    var form = $("#updatedUser");
    var formData = form.serialize();
    let url = form.attr("action")
    var headers = {
        "Authorization": "Bearer " + token
    };

    $.ajax({
        url: "/v1" + url,
        type: "POST",
        data: formData,
        headers: headers,
        dataType: "html",
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
};

async function deleteAccount() {
    var token = localStorage.getItem('token') // Replace with the actual token value
    var form = $("#userDetails");
    var formData = form.serialize();
    let url = form.attr("action")
    // await apiCall(url,'POST',formData)
    var headers = {
        "Authorization": "Bearer " + token
    };

    $.ajax({
        url: "/v1/" + url,
        type: "POST",
        data: formData,
        headers: headers,
        dataType: "html",
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
};

async function updateForm() {
    var token = localStorage.getItem('token') // Replace with the actual token value
    var form = $("#updateDetails");
    var formData = form.serialize();
    let url = form.attr("action")
    var headers = {
        "Authorization": "Bearer " + token
    };

    $.ajax({
        url: "/v1" + url,
        type: "POST",
        data: formData,
        headers: headers,
        dataType: "html",
        success: function (data) {
            document.open();
            document.write(data);
            document.close();
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}

function getCookie(name) {
    const cookies = document.cookie.split(';');
    console.log("++++",document.cookie)

    for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        if (cookie.startsWith(name + '=')) {
            return cookie.substring(name.length + 1);
        }
    }
    return null; // Cookie not found
}

