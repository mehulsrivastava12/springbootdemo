package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CourseVideo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long videoId;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
    @JoinColumn(name = "course_id")
    private Course course;
    private String title;
    private String description;
    private String link;
    @OneToMany(mappedBy = "courseVideo", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonBackReference
    private List<CourseDocuments> courseDocuments;

    @Override
    public String toString() {
        return "CourseVideo{" +
                "videoId=" + videoId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

//    @Column(nullable = true)
//    private String fileName;
//    @Lob
//    @Column(nullable = true)
//    private byte[] fileContent;

}
