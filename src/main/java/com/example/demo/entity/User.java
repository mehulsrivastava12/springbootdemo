package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long uid;
    private String firstName;
    private String lastName;
    private String dob;
    private String email;
    private String password;
    private String phoneNumber;

    @Column(nullable = true)
    private String otp;

}
