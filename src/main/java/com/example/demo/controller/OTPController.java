package com.example.demo.controller;

import com.example.demo.DTO.OtpResponseDTO;
import com.example.demo.DTO.OtpStatus;
import com.example.demo.entity.Course;
import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import com.example.demo.jwt.JwtUtil;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.OTPService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1")
public class OTPController {

    @Autowired
    private OTPService otpService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    UserService userService;
    @Autowired
    JwtUtil jwtUtil;

    @RequestMapping("/loginUsingOTP")
    private String loginUsingOTP() {
        return "loginUsingOTP";
    }

    @PostMapping("/sendOTP")
    public String sendOTP(@RequestParam("phoneNumber") String phoneNumber, Model model) {
        User user = userRepository.findByPhoneNumber(phoneNumber);
        Instructor instructor = instructorRepository.findByPhoneNumber(phoneNumber);
        if (user != null) {
            OtpResponseDTO otpResponseDTO = otpService.sendOTP(phoneNumber, user, null);
            if (otpResponseDTO.getStatus().equals(OtpStatus.DELIVERED)) {
                model.addAttribute("email", user.getEmail());
                return "verifyOTP";
            } else {
                model.addAttribute("msg", "OTP cannot be sent due to some technical problem");
                return "loginUsingOTP";
            }
        } else if (instructor != null) {
            OtpResponseDTO otpResponseDTO = otpService.sendOTP(phoneNumber, null, instructor);
            if (otpResponseDTO.getStatus().equals(OtpStatus.DELIVERED)) {
                model.addAttribute("email", instructor.getEmail());
                return "verifyOTP";
            } else {
                model.addAttribute("msg", "OTP cannot be sent due to some technical problem");
                return "loginUsingOTP";
            }
        } else {
            model.addAttribute("msg", "No user exist with this phoneNumber");
            return "loginUsingOTP";
        }
    }

    @PostMapping("/verifyOTP")
    public String verifyOTP(@RequestParam("otp") String otp, @RequestParam("email") String email, Model model, @RequestParam("usingEmail") boolean usingEmail) {
        User user = userRepository.findByEmail(email);
        Instructor instructor = instructorRepository.findByEmail(email);
        String otpValid = otpService.verifyOTP(otp, user, instructor);
        model.addAttribute("email", email);
        if (otpValid.equals("OTP is Valid")) {
            if (usingEmail) {
                return "newPassword";
            }
            String token = jwtUtil.generateToken(user);
            List<Course> allCourses = userService.getCourses();
            model.addAttribute("token", token);
            model.addAttribute("allCourses", allCourses);
            return "userhome";
        } else {
            model.addAttribute("msg", "OTP is INVALID");
            return "verifyOTP";
        }
    }

    @PostMapping("/forgotPasswordOTP")
    public String forgotPasswordOTP(@RequestParam("email") String email, Model model) {
        User user = userRepository.findByEmail(email);
        Instructor instructor = instructorRepository.findByEmail(email);

        if (user != null || instructor != null) {
            if (user != null) {
                OtpResponseDTO otpResponseDTO = otpService.sendEmailOTP(email, user, null);
                model.addAttribute("email", user.getEmail());
                model.addAttribute("usingEmail", true);
            } else {
                OtpResponseDTO otpResponseDTO = otpService.sendEmailOTP(email, null, instructor);
                model.addAttribute("email", instructor.getEmail());
                model.addAttribute("usingEmail", true);
            }
            return "verifyOTP";
        } else {
            model.addAttribute("msg", "No account exist with this email");
            return "forgotPassword";
        }
    }

    @PostMapping("/changePassword")
    public String changePassword(@RequestParam("newPassword") String password, @RequestParam("email") String email) {
        User user = userRepository.findByEmail(email);
        Instructor instructor = instructorRepository.findByEmail(email);
        if (user != null) {
            user.setPassword(new BCryptPasswordEncoder().encode(password));
            userRepository.save(user);
        } else {
            instructor.setPassword(new BCryptPasswordEncoder().encode(password));
            instructorRepository.save(instructor);
        }
        return "redirect:/v1/";
    }
}
