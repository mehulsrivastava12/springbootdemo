package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.jwt.JwtUtil;
import com.example.demo.entity.Course;
import com.example.demo.entity.Instructor;
import com.example.demo.service.InstructorService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/v1")
public class InstructorController {

    @Autowired
    private InstructorService instructorService;
    @Autowired
    JwtUtil jwtUtil;

    @RequestMapping("/instructorregister")
    public String addInstructor(@ModelAttribute Instructor instructor) {
        instructor.setPassword(new BCryptPasswordEncoder().encode(instructor.getPassword()));
        instructorService.createInstructor(instructor);
        return "loginInstructor";
    }

    @RequestMapping("/deleteinstructor/{instructorId}")
    public RedirectView deleteInstructor(@PathVariable("instructorId") int instructorId, HttpServletRequest request) {
        this.instructorService.deleteInstructor(instructorId);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(request.getContextPath() + "/v1/");
        return redirectView;
    }

    @RequestMapping("/instructorDetails/{instructorId}")
    public String instructorDetails(@PathVariable("instructorId") long instructorId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Instructor details = instructorService.getInstructor((int) instructorId);
            model.addAttribute("instructor", details);
            model.addAttribute("id", instructorId);
            return "instructorDetail";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/instructorcourse/{instructorId}")
    public String instructorCourse(@PathVariable("instructorId") int instructorId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Course> instructorCourse = this.instructorService.getInstructorCourse(instructorId);
            model.addAttribute("instructorCourse", instructorCourse);
            model.addAttribute("id", instructorId);
            return "instructorCourse";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/updateInstructorForm/{id}")
    public String Instructor(@PathVariable("id") int id, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Instructor instructor = instructorService.getInstructor(id);
            model.addAttribute("instructor", instructor);
            model.addAttribute("id", id);
            return "update_instructor_form";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @PostMapping("/updateInstructor/{id}")
    public String updateInstructor(@PathVariable("id") long instructorId, @ModelAttribute Instructor instructor, @RequestHeader("Authorization") String authorizationHeader, Model model) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Instructor existingInstructor = instructorService.getInstructor(instructorId);
            existingInstructor.setEmail(instructor.getEmail());
            existingInstructor.setFirstName(instructor.getFirstName());
            existingInstructor.setLastName(instructor.getLastName());
            existingInstructor.setDob(instructor.getDob());
            existingInstructor.setPhoneNumber(instructor.getPhoneNumber());
            instructorService.createInstructor(existingInstructor);
            model.addAttribute("id", existingInstructor.getId());
            return "instructorDetail";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/searchmy/{id}/{title}")
    public String search(@PathVariable("title") String title, @PathVariable("id") String id, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Course> searchCourses = instructorService.searchMyCourse(title);
            model.addAttribute("searchCourses", searchCourses);
            model.addAttribute("id", id);
            return "searchCourses";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/addcourse/{id}")
    private String course(@PathVariable("id") String instructorId, Model m, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            m.addAttribute("id", instructorId);
            return "addcourse";
        } else {
            m.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @GetMapping("/instructor/{id}")
    public ResponseEntity<String> getUser(@PathVariable("id") long instructorId) {
        Instructor instructor = instructorService.getInstructor(instructorId);
        if (instructor != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String userJson = objectMapper.writeValueAsString(instructor);
                return ResponseEntity.ok(userJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
