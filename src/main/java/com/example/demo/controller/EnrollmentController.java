package com.example.demo.controller;

import com.example.demo.entity.UserEnrollment;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.MyOrderRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;


@Controller
@RequestMapping("/v1")
public class EnrollmentController {

    @Autowired
    private EnrollmentService enrollmentService;
    @Autowired
    MyOrderRepository myOrderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    InstructorRepository instructorRepository;

    @RequestMapping("/enroll/{uid}")
    public String enrollUser(@PathVariable("uid") long uid, @RequestBody Map<String, Object> data, Model model) {
        UserEnrollment userEnrollment = new UserEnrollment();
        userEnrollment.setCourse(courseRepository.findById(Long.valueOf(data.get("courseId").toString())).get());
        userEnrollment.setUser(userRepository.findById(uid).get());
        userEnrollment.setInstructor(instructorRepository.findById(Long.valueOf(data.get("instructorId").toString())).get());
        userEnrollment.setDate(new Date());
        model.addAttribute("id", uid);
        this.enrollmentService.enroll(userEnrollment);
        return "successfully";
    }

}
