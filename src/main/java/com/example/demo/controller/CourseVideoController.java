package com.example.demo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.entity.Course;
import com.example.demo.entity.CourseDocuments;
import com.example.demo.jwt.JwtUtil;
import com.example.demo.entity.CourseVideo;
import com.example.demo.repository.CourseDocumentsRepository;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.CourseVideoRepository;
import com.example.demo.service.CoursesVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/v1")
public class CourseVideoController {
    @Autowired
    private CoursesVideoService coursesVideoService;
    @Autowired
    CourseVideoRepository courseVideoRepository;
    @Autowired
    CourseDocumentsRepository courseDocumentsRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    JwtUtil jwtUtil;

    @RequestMapping("/addvideo/{id}")
    public String addVideo(@RequestParam("course") long courseId,
                           @RequestParam("title") String title,
                           @RequestParam("description") String description,
                           @RequestParam("link") String link,
                           @RequestParam("files") MultipartFile[] files,
                           Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
                           @RequestHeader("Authorization") String authorizationHeader) throws IOException {
        if (jwtUtil.validateToken(authorizationHeader)) {
            CourseVideo courseVideo = coursesVideoService.createVideo(courseId, title, description, link);
            for (MultipartFile file : files) {
                CourseDocuments courseDocuments = new CourseDocuments();
                courseDocuments.setFileName(file.getOriginalFilename());
                courseDocuments.setFileContent(file.getBytes());
                courseDocuments.setCourseVideo(courseVideo);
                courseDocumentsRepository.save(courseDocuments);
            }
            String redirectUrl = request.getContextPath() + "/v1/mycoursevideo/" + courseVideo.getCourse().getCid();
            redirectAttributes.addAttribute("authorizationHeader", authorizationHeader);
            model.addAttribute("cid", courseVideo.getCourse().getCid());
            model.addAttribute("id", courseVideo.getCourse().getInstructor().getId());
            return "redirect:" + redirectUrl;
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/deletevideo/{videoId}")
    public String deleteVideo(@PathVariable("videoId") long videoId, HttpServletRequest request, RedirectAttributes redirectAttributes, @RequestHeader("Authorization") String authorizationHeader, Model model) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            coursesVideoService.deleteVideo(videoId);
            String redirectUrl = request.getContextPath() + "/";
            redirectAttributes.addAttribute("authorizationHeader", authorizationHeader);
            model.addAttribute("id", courseVideoRepository.getById(videoId).getCourse().getInstructor().getId());
            return "redirect:" + redirectUrl;
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/mycoursevideo/{cid}")
    public String getVideo(@PathVariable("cid") long cid, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Course course = courseRepository.findById(cid).get();
            List<CourseVideo> courseVideos = course.getCourseVideos();
            model.addAttribute("courseVideos", courseVideos);
            model.addAttribute("id", courseRepository.getById(cid).getInstructor().getId());
            return "myvideos";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @RequestMapping("/addvideoform/{cid}")
    public String addvideoform(@PathVariable("cid") long cid, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            model.addAttribute("id", courseRepository.getById(cid).getInstructor().getId());
            return "addvideo";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }

    @GetMapping("/downloadFile/{fileName}/{videoId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("fileName") String fileName, @PathVariable("videoId") long videoId) {
        CourseVideo courseVideo = courseVideoRepository.findById(videoId).get();
        CourseDocuments courseDocument = courseDocumentsRepository.findByFileNameAndCourseVideo(fileName, courseVideo);

        if (courseDocument != null) {
            ByteArrayResource resource = new ByteArrayResource(courseDocument.getFileContent());

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                    .contentLength(courseDocument.getFileContent().length)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
