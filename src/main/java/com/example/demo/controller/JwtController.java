package com.example.demo.controller;

import com.example.demo.DTO.JwtRequest;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import com.example.demo.jwt.JwtUtil;

@RestController
public class JwtController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    JwtService jwtService;

    @PostMapping(value = "/token")
    public String generateToken(@RequestBody JwtRequest jwtRequest) throws Exception {
        return jwtService.generateToken(jwtRequest);
    }
}
