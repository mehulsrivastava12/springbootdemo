package com.example.demo.controller;

import com.example.demo.entity.MyOrder;
import com.example.demo.repository.MyOrderRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.razorpay.*;

import java.util.Map;

@Controller
@RequestMapping("/v1")
public class PaymentController {
    @Autowired
    MyOrderRepository myOrderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaymentService paymentService;

    @PostMapping("/create_order")
    @ResponseBody
    public String createOrder(@RequestBody Map<String, Object> data) throws RazorpayException {
        Order order = paymentService.createOrder(data);
        paymentService.transactionDetails(data, order);
        return order.toString();
    }

    @PostMapping("/updatePayment")
    public void updatePayment(@RequestBody Map<String, Object> data) {
        MyOrder myOrder1 = myOrderRepository.findByOrderId(data.get("orderId").toString());
        String status = data.get("status").toString();
        if (status.equals("SUCCESS")) {
            myOrder1.setPaymentId(data.get("paymentId").toString());
        }
        myOrder1.setStatus(status);
        myOrderRepository.save(myOrder1);
    }
}
