package com.example.demo.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.example.demo.entity.*;
import com.example.demo.jwt.JwtUtil;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.InstructorService;
import com.example.demo.service.UserService;
import com.example.demo.DTO.EnrollDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/v1")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private InstructorService instructorService;
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CourseRepository courseRepository;

    @RequestMapping("/register")
    public String addUser(@ModelAttribute User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userService.createUser(user);
        return "login";
    }

    @RequestMapping("/detail/{userId}")
    public String userDetail(@PathVariable("userId") long userId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            User detail = this.userService.getUser(userId);
            model.addAttribute("user", detail);
            model.addAttribute("id", userId);
            return "userDetail";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @GetMapping("/mycourses/{userId}")
    public String myCourse(@PathVariable("userId") int userId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<EnrollDTO> myCourse = userService.getMyCourse(userId);
            model.addAttribute("myCourse", myCourse);
            model.addAttribute("id", userId);
            return "myCourse";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/usercoursevideo/{cid}/{userId}")
    public String myVideo(@PathVariable("cid") long cid, @PathVariable("userId") int userId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Course course = courseRepository.findById(cid).get();
            List<CourseVideo> courseVideos = course.getCourseVideos();
            model.addAttribute("myVideo", courseVideos);
            model.addAttribute("id", userId);
            return "usercoursevideo";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/allCourses/{uid}")
    public String allCourse(@PathVariable("uid") int uid, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Course> allCourses = userService.getCourses();
            model.addAttribute("allCourses", allCourses);
            model.addAttribute("id", uid);
            return "allCourses";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/search/{title}")
    public String search(@PathVariable("title") String title/*,@PathVariable("uid") String uid*/, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Course> searchCourses = userService.searchCourse(title);
            model.addAttribute("searchCourses", searchCourses);
            return "searchCourses";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/searchinstructor/{instructor}")
    public String searchInstructor(@PathVariable("instructor") String firstname/*,@PathVariable("uid") String uid*/, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Instructor> searchInstructor = userService.searchInstructor(firstname);
            if (searchInstructor.size() == 0) {
                return null;
            } else {
                model.addAttribute("searchInstructor", searchInstructor);
                return "searchInstructor";
            }
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/searchinstructorcourse/{id}")
    public String searchInstructorCourse(@PathVariable("id") int id/*,@PathVariable("uid") int uid,*/, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            List<Course> searchinstructorcourse = this.instructorService.getInstructorCourse(id);
            model.addAttribute("searchinstructorcourse", searchinstructorcourse);
            return "searchinstructorcourse";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/delete/{uid}")
    public String deleteUser(@PathVariable("uid") int uid, HttpServletRequest request, @RequestHeader("Authorization") String authorizationHeader, Model model) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            this.userService.deleteUser(uid);
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
        }
        return "login";
    }

    @RequestMapping("/updateForm/{uid}")
    public String updateForm(@PathVariable("uid") long uid, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            User user = this.userService.getUser(uid);
            model.addAttribute("user", user);
            model.addAttribute("id", uid);
            return "update_form";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @PostMapping("/updateDetails/{id}")
    public String updateDetails(@PathVariable("id") long userId, @ModelAttribute User user, @RequestHeader("Authorization") String authorizationHeader, Model model) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            User existingUser = userRepository.findById(userId).get();
            existingUser.setDob(user.getDob());
            existingUser.setEmail(user.getEmail());
            existingUser.setFirstName(user.getFirstName());
            existingUser.setLastName(user.getLastName());
            existingUser.setPhoneNumber(user.getPhoneNumber());
            userRepository.save(existingUser);
            model.addAttribute("id", existingUser.getUid());
            return "userDetail";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<String> getUser(@PathVariable("userId") long userId) {
        User user = userService.getUser(userId);
        if (user != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String userJson = objectMapper.writeValueAsString(user);
                return ResponseEntity.ok(userJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
