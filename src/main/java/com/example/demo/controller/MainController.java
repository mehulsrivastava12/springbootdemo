package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.DTO.JwtRequest;
import com.example.demo.jwt.JwtUtil;
import com.example.demo.entity.Course;
import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/v1")
public class MainController {
    @Autowired
    UserService userService;
    @Autowired
    InstructorService instructorService;
    @Autowired
    CourseService courseService;
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    JwtService jwtService;
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MainService mainService;

    @Value("${RAZORPAY_KEY}")
    private String key;

    @RequestMapping("/")
    private String login() {
        return "login";
    }

    @RequestMapping("/logout")
    private String logout(HttpServletResponse response) {
        mainService.logout(response);
        return "login";
    }

    @RequestMapping("/signup")
    private String signup() {
        return "signup";
    }

    @RequestMapping("/loginInstructor")
    private String loginInstructor() {
        return "loginInstructor";
    }

    @RequestMapping("/signupInstructor")
    private String signupInstructor() {
        return "signupInstructor";
    }

    @RequestMapping("/forgotPassword")
    private String forgotPassword() {
        return "forgotPassword";
    }

    @RequestMapping("/userToken")
    private String userLogin(@RequestParam("email") String email, @RequestParam("password") String password, Model model, HttpServletResponse httpServletResponse) {
        ResponseEntity<String> response = mainService.userLogin(email,password,httpServletResponse);
        if(response.getStatusCode().is2xxSuccessful()){
            return "redirect:/v1/userhome";
        }
        else {
            model.addAttribute("msg", "Invalid Username Or Password");
            return "login";
        }
    }

    @PostMapping("/instructorToken")
    private String instructorLogin(@RequestParam("email") String email, @RequestParam("password") String password, Model model,HttpServletResponse httpServletResponse) {
        ResponseEntity<String> response = mainService.instructorLogin(email,password,httpServletResponse);
        if(response.getStatusCode().is2xxSuccessful()){
            return "redirect:/v1/instructorhome";
        }
        else {
            model.addAttribute("msg", "Invalid Username Or Password");
            return "loginInstructor";
        }
    }

    @GetMapping("/userhome")
    private String userPage(Model model, HttpServletRequest request,@CookieValue(name = "jwtToken", required = false) String jwtToken) {
//        Cookie[] cookies = request.getCookies();
//        String authorizationHeader = null;
//        if (cookies != null) {
//            for (Cookie cookie : cookies) {
//                if ("jwtToken".equals(cookie.getName())) { // Replace with your cookie name
//                    authorizationHeader = cookie.getValue();
                    if (jwtUtil.validateToken(jwtToken)) {
                        User user = jwtUtil.extractUserDetailsFromToken(jwtToken);
                        model.addAttribute("id", user.getUid());
                        List<Course> allCourses = userService.getCourses();
                        model.addAttribute("allCourses", allCourses);
                        model.addAttribute("key", key);
                        model.addAttribute("token",jwtToken);
                        return "userhome";
                    }
//                    break;
//                }
//            }
//        }
        model.addAttribute("msg", "Session Expired Login Again");
        return "login";
    }

    @GetMapping("/instructorhome")
    private String instructorPage(Model model,HttpServletRequest request,@CookieValue(name = "jwtToken", required = false) String jwtToken) {
//        Cookie[] cookies = request.getCookies();
//        String authorizationHeader = null;

//        if (cookies != null) {
//            for (Cookie cookie : cookies) {
//                if ("jwtToken".equals(cookie.getName())) { // Replace with your cookie name
//                    authorizationHeader = cookie.getValue();
                    if (jwtUtil.validateToken(jwtToken)) {
                        Instructor instructor = jwtUtil.extractInstructorDetailsFromToken(jwtToken);
                        List<Course> instructorCourse = instructorService.getInstructorCourse(instructor.getId());
                        model.addAttribute("id", instructor.getId());
                        model.addAttribute("instructorCourse", instructorCourse);
                        model.addAttribute("token", jwtToken);
                        return "instructorhome";
                    }
//                    break;
//                }
//            }
//        }
        model.addAttribute("msg", "Session Expired Login Again");
        return "loginInstructor";
    }

    @RequestMapping("/search")
    private RedirectView searchinstructor(/*@PathVariable("uid") int uid,*/@RequestParam("instructor") String firstName, Model m, HttpServletRequest request, @RequestHeader("Authorization") String authorizationHeader) {
        RedirectView redirectView = new RedirectView();
        try {
            if (jwtUtil.validateToken(authorizationHeader)) {
                int count = instructorRepository.countByFirstName(firstName);
                int c = courseRepository.countByTitle(firstName);
                if (count >= 1) {
                    String url = "searchinstructor/" + firstName;
                    redirectView.setUrl(request.getContextPath() + "/" + url);
                    return redirectView;
                } else if (c >= 1) {
                    String url = "search/" + firstName;
                    redirectView.setUrl(request.getContextPath() + "/" + url);
                    return redirectView;
                } else {
                    String url = "nodata";
                    redirectView.setUrl(request.getContextPath() + "/" + url);
                    return redirectView;
                }
            } else {
                m.addAttribute("msg", "Session Expired Login Again");
                redirectView.setUrl(request.getContextPath() + "/login");
                return redirectView;
            }

        } catch (Exception e) {
            String url = "nodata";
            redirectView.setUrl(request.getContextPath() + "/" + url);
            return redirectView;
        }
    }

    @RequestMapping("/nodata")
    private String nodata() {
        return "nodata";
    }

    @GetMapping("/checkServerStatus")
    public ResponseEntity<String> checkServerStatus() {
        return ResponseEntity.ok("Server is running.");
    }

}
