package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.jwt.JwtUtil;
import com.example.demo.entity.Course;
import com.example.demo.repository.CourseRepository;
import com.example.demo.service.CourseService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/v1")
public class CourseController {

    @Autowired
    private CourseService courseService;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    JwtUtil jwtUtil;

    @RequestMapping("/course/{courseId}")
    public String getCourses(@PathVariable("courseId") long courseId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            Course course = courseService.getCourse(courseId);
            model.addAttribute("id", courseRepository.getById(courseId).getInstructor().getId());
            model.addAttribute("course", course);
            return "course";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "login";
        }
    }

    @RequestMapping("/createcourse/{id}")
    public RedirectView addCourse(@PathVariable("id") int id, @ModelAttribute Course course, Model model, HttpServletRequest request, @RequestHeader("Authorization") String authorizationHeader) {
        RedirectView redirectView = new RedirectView();
        if (jwtUtil.validateToken(authorizationHeader)) {
            courseService.createCourse(course);
            model.addAttribute("cid", course.getCid());
            model.addAttribute("id", id);
            redirectView.setUrl(request.getContextPath() + "/v1/addvideoform/" + course.getCid());
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            redirectView.setUrl(request.getContextPath() + "/loginInstructor/");
        }
        return redirectView;
    }

    @RequestMapping("/deletecourse/{courseId}")
    public String removeCourse(@PathVariable("courseId") long courseId, Model model, @RequestHeader("Authorization") String authorizationHeader) {
        if (jwtUtil.validateToken(authorizationHeader)) {
            model.addAttribute("id", courseRepository.getById(courseId).getInstructor().getId());
            courseService.deleteCourse(courseId);
            return "instructorCourses";
        } else {
            model.addAttribute("msg", "Session Expired Login Again");
            return "loginInstructor";
        }
    }
}
