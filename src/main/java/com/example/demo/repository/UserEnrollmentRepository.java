package com.example.demo.repository;

import com.example.demo.entity.User;
import com.example.demo.entity.UserEnrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserEnrollmentRepository extends JpaRepository<UserEnrollment, Long> {

    @Query(
            value = "CALL GetMyCourseData(:userId)",
            nativeQuery = true
    )
    List<Object[]> getMyCourse(@Param("userId") User user);
}
