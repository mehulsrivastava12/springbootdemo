package com.example.demo.repository;

import com.example.demo.entity.Course;
import com.example.demo.entity.CourseVideo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseVideoRepository extends JpaRepository<CourseVideo, Long> {
    List<CourseVideo> findAllByCourse(Course course);
}
