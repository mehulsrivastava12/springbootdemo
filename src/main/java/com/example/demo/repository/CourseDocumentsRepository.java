package com.example.demo.repository;

import com.example.demo.entity.CourseDocuments;
import com.example.demo.entity.CourseVideo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseDocumentsRepository extends JpaRepository<CourseDocuments, Long> {
    CourseDocuments findByFileNameAndCourseVideo(String fileName, CourseVideo courseVideo);
}
