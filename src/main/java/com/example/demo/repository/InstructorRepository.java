package com.example.demo.repository;

import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InstructorRepository extends JpaRepository<Instructor, Long> {
    Instructor findByEmail(String email);

    List<Instructor> findAllByFirstName(String firstName);

    int countByFirstName(String firstName);

    Instructor findByPhoneNumber(String phoneNumber);

}
