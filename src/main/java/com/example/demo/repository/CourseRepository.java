package com.example.demo.repository;

import com.example.demo.entity.Course;
import com.example.demo.entity.Instructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByTitle(String title);

    List<Course> findAllByInstructor(Instructor instructor);

    int countByTitle(String title);

}
