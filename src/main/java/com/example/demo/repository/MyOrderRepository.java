
package com.example.demo.repository;

import com.example.demo.entity.MyOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyOrderRepository extends JpaRepository<MyOrder, Long> {
    public MyOrder findByOrderId(String orderId);

}
