package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationFilter jwtFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
//		.cors().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
//				.antMatchers("/token").permitAll()
//				.antMatchers("/v1/").permitAll()
//				.antMatchers("/v1/logout").permitAll()
//				.antMatchers("/v1/signup").permitAll()
//				.antMatchers("/v1/register").permitAll()
//				.antMatchers("/v1/signupInstructor").permitAll()
//				.antMatchers("/v1/instructorregister").permitAll()
//				.antMatchers("/v1/userhome").permitAll()
//				.antMatchers("/v1/instructorhome").permitAll()
//				.antMatchers("/v1/userToken").permitAll()
//				.antMatchers("/v1/instructorToken").permitAll()
//				.antMatchers("/v1/signup").permitAll()
//				.antMatchers("/v1/loginInstructor").permitAll()
//				.antMatchers("/v1/signupInstructor").permitAll()
//				.antMatchers("/v1/user/**").permitAll()
//				.antMatchers("/v1/instructor/**").permitAll()
//		//		.antMatchers("/v1/create_order").permitAll()
//		//		.antMatchers("/v1/updatePayment").permitAll()
//		//		.antMatchers("/v1/enroll/**").permitAll()
//		//		.antMatchers("/v1/nodata").permitAll()
//		      .antMatchers("/script.js").permitAll()
		//      .antMatchers("/v1/sendOTP").permitAll()
		//      .antMatchers("/v1/verifyOTP").permitAll()
		//      .antMatchers("/v1/verifyOTP").permitAll()
		//      .antMatchers("/v1/downloadFile/**").permitAll()
		//      .antMatchers("/v1/loginUsingOTP").permitAll()
		//      .antMatchers("/v1/user/**").permitAll()
		//      .antMatchers("/v1/instructor/**").permitAll()
				.anyRequest().permitAll()
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);



//		http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


}

