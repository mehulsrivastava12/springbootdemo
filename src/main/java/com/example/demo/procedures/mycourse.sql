DROP procedure IF EXISTS GetMyCourseData;

DELIMITER $$
CREATE PROCEDURE GetMyCourseData(IN userId BIGINT)
BEGIN
    SELECT
        ue.enrollment_id AS enrollmentId,
        c.title AS title,
        i.first_name AS firstName,
        i.last_name AS lastName,
        ue.date AS date,
        cid AS course
    FROM
        user_enrollment ue
            INNER JOIN
        course c ON ue.course_cid = cid
            INNER JOIN
        instructor i ON c.instructor_id = id
    WHERE
            ue.user_uid = userId;
END$$
DELIMITER ;
