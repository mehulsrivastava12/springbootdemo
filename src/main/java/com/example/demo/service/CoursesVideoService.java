package com.example.demo.service;

import com.example.demo.entity.Course;
import com.example.demo.entity.CourseVideo;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.CourseVideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class CoursesVideoService {

    @Autowired
    CourseVideoRepository courseVideoRepository;
    @Autowired
    CourseRepository courseRepository;

    public void deleteVideo(long videoId) {
        courseVideoRepository.deleteById(videoId);
    }

    public List<CourseVideo> getVideo(long courseId) {
        Course course = courseRepository.findById(courseId).get();
        List<CourseVideo> courseVideo = courseVideoRepository.findAllByCourse(course);
        return courseVideo;
    }

    @Transactional
    public CourseVideo createVideo(long courseId, String title, String description, String link) {
        Course course = courseRepository.findById(courseId).get();
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setCourse(course);
        courseVideo.setTitle(title);
        courseVideo.setDescription(description);
        courseVideo.setLink(link);
        return courseVideoRepository.save(courseVideo);
    }
}
