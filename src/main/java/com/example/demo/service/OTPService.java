package com.example.demo.service;

import com.example.demo.DTO.OtpResponseDTO;
import com.example.demo.DTO.OtpStatus;
import com.example.demo.config.TwilioConfig;
import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserRepository;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

@Service
public class OTPService {

    @Autowired
    TwilioConfig twilioConfig;
    @Autowired
    UserRepository userRepository;
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    EmailService emailService;

    Map<String, String> otpMap = new HashMap<>();

    public OtpResponseDTO sendOTP(String phoneNumber, User user, Instructor instructor) {
        OtpResponseDTO otpResponseDTO = null;
        try {
            PhoneNumber to = new PhoneNumber(phoneNumber);
            PhoneNumber from = new PhoneNumber((twilioConfig.getPhoneNumber()));
            String otp = generateOTP();
            String otpMessage = "Dear Customer, Your OTP for ONLEARNING login is " + otp;
            Message message = Message.creator(to, from, otpMessage).create();
            user.setOtp(otp);
            userRepository.save(user);
            otpResponseDTO = new OtpResponseDTO(OtpStatus.DELIVERED, otpMessage);
        } catch (Exception e) {
            otpResponseDTO = new OtpResponseDTO(OtpStatus.FAILED, e.getMessage());
            e.printStackTrace();
        }
        return otpResponseDTO;
    }

    public String verifyOTP(String otp, User user, Instructor instructor) {
        if (user != null) {
            if (user.getOtp().equals(otp)) {
                return "OTP is Valid";
            }
        } else if (instructor != null) {
            if (instructor.getOtp().equals(otp)) {
                return "OTP is Valid";
            }
        }
        return "OTP is INVALID";
    }

    public OtpResponseDTO sendEmailOTP(String email, User user, Instructor instructor) {
        OtpResponseDTO otpResponseDTO = null;
        try {
            String otp = generateOTP();
            String subject = "OTP from Onlearning";
            String otpMessage = "Dear Customer, Your OTP for ONLEARNING login is " + otp;
            System.out.println("+OTP" + otp);
//            boolean flag = emailService.sendEmail(subject,otpMessage,email);
            if (user != null) {
                user.setOtp(otp);
                userRepository.save(user);
            } else {
                instructor.setOtp(otp);
                instructorRepository.save(instructor);
            }
            otpResponseDTO = new OtpResponseDTO(OtpStatus.DELIVERED, otpMessage);
        } catch (Exception e) {
            otpResponseDTO = new OtpResponseDTO(OtpStatus.FAILED, e.getMessage());
            e.printStackTrace();
        }
        return otpResponseDTO;
    }

    public String generateOTP() {
        return new DecimalFormat("000000")
                .format(new Random().nextInt(999999));
    }
}
