package com.example.demo.service;

import com.example.demo.entity.Course;
import com.example.demo.entity.Instructor;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class InstructorService {


    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    CourseRepository courseRepository;

    public void deleteInstructor(long instructorid) {
        instructorRepository.delete(instructorRepository.findById(instructorid).get());
    }

    public List<Course> getInstructorCourse(long instructorId) {
        Instructor instructor = getInstructor(instructorId);
        List<Course> instructorCourse = courseRepository.findAllByInstructor(instructor);
        return instructorCourse;
    }

    public List<Course> searchMyCourse(String title) {
        List<Course> allCourses = courseRepository.findAllByTitle(title);
        return allCourses;
    }

    @Transactional
    public void createInstructor(Instructor instructor) {
        instructorRepository.save(instructor);
    }

    public Instructor getInstructor(long id) {
        return instructorRepository.findById(id).get();
    }

    @Transactional
    public void addCourse(Course course) {
        courseRepository.save(course);
    }

    @Transactional
    public void removeCourse(int cid) {
        Course c = courseRepository.getById((long) cid);
        courseRepository.delete(c);
    }
}
