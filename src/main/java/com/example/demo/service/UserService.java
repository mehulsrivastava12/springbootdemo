package com.example.demo.service;

import com.example.demo.entity.*;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserEnrollmentRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.DTO.EnrollDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserEnrollmentRepository userEnrollmentRepository;
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    CourseRepository courseRepository;

    public void deleteUser(long uid) {
        userRepository.deleteById(uid);
    }

    public User getUser(long uid) {
        User user = userRepository.findById(uid).get();
        return user;
    }

    public List<Course> getCourses() {
        List<Course> courses = courseRepository.findAll();
        return courses;
    }

    public List<EnrollDTO> getMyCourse(long uid) {
        User user = getUser(uid);
        List<Object[]> result = userEnrollmentRepository.getMyCourse(user);
        return mapToEnrollDTOList(result);
    }

    public List<Course> searchCourse(String title) {
        List<Course> allCourses = courseRepository.findAllByTitle(title);
        return allCourses;
    }

    public List<Instructor> searchInstructor(String instructor) {
        List<Instructor> instructors = instructorRepository.findAllByFirstName(instructor);
        return instructors;
    }

    @Transactional
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Transactional
    public void enroll(UserEnrollment userEnrollment) {
        userEnrollmentRepository.save(userEnrollment);
    }

    public List<EnrollDTO> mapToEnrollDTOList(List<Object[]> resultList) {
        List<EnrollDTO> enrollDTOList = new ArrayList<>();

        for (Object[] row : resultList) {
            Long enrollmentId = ((BigInteger) row[0]).longValue(); // Change to BigInteger and longValue() if needed
            String title = (String) row[1];
            String firstName = (String) row[2];
            String lastName = (String) row[3];
            Date date = (Date) row[4];
            Long course = ((BigInteger) row[5]).longValue();

            EnrollDTO enrollDTO = new EnrollDTO(enrollmentId, title, firstName, lastName, date,course);
            enrollDTOList.add(enrollDTO);
        }

        return enrollDTOList;
    }
}
