package com.example.demo.service;

import com.example.demo.entity.Course;
import com.example.demo.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Transactional
    public void createCourse(Course course) {
        courseRepository.save(course);
    }

    @Transactional
    public void deleteCourse(long cid) {
        Course c = courseRepository.getById(cid);
        courseRepository.delete(c);
    }

    public Course getCourse(long cid) {
        return courseRepository.getById(cid);
    }
}
