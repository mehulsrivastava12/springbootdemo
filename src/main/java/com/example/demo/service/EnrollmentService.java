package com.example.demo.service;

import com.example.demo.entity.UserEnrollment;
import com.example.demo.repository.UserEnrollmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class EnrollmentService {

    @Autowired
    UserEnrollmentRepository userEnrollmentRepository;

    @Transactional
    public void enroll(UserEnrollment userEnrollment) {
        userEnrollmentRepository.save(userEnrollment);
    }

}
