package com.example.demo.service;

import com.example.demo.entity.MyOrder;
import com.example.demo.entity.User;
import com.example.demo.repository.MyOrderRepository;
import com.example.demo.repository.UserRepository;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;

@Service
public class PaymentService {

    @Value("${RAZORPAY_KEY}")
    private String key;
    @Value("${RAZORPAY_SECRET_KEY}")
    private String secretKey;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MyOrderRepository myOrderRepository;

    public Order createOrder(Map<String, Object> data) throws RazorpayException {
        int amount = Integer.parseInt(data.get("amount").toString());
        RazorpayClient razorpayClient = new RazorpayClient(key, secretKey);
        JSONObject orderRequest = new JSONObject();
        orderRequest.put("amount", amount * 100); // amount in the smallest currency unit
        orderRequest.put("currency", "INR");
        orderRequest.put("receipt", "txn_" + generateRandomNumber());
        Order order = razorpayClient.orders.create(orderRequest);
        return order;
    }

    public void transactionDetails(Map<String, Object> data, Order order) {
        long userId = Long.valueOf(data.get("id").toString());
        User user = userRepository.findById(userId).get();
        MyOrder myOrder = new MyOrder();
        myOrder.setAmount(order.get("amount"));
        myOrder.setOrderId(order.get("id"));
        myOrder.setStatus("CREATED");
        myOrder.setUser(user);
        myOrder.setReceipt(order.get("receipt"));
        myOrderRepository.save(myOrder);
    }

    public static int generateRandomNumber() {
        int min = 100000;
        int max = 999999;
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
