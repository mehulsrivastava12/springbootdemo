package com.example.demo.service;

import com.example.demo.DTO.JwtRequest;
import com.example.demo.jwt.JwtUtil;
import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class JwtService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    InstructorRepository instructorRepository;
    @Autowired
    JwtUtil jwtUtil;

    public String generateToken(@RequestBody JwtRequest jwtRequest) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = userRepository.findByEmail(jwtRequest.getUsername());
        Instructor instructor = null;
        if (user == null) {
            instructor = instructorRepository.findByEmail(jwtRequest.getUsername());
        }
        if (user != null && encoder.matches(jwtRequest.getPassword(), user.getPassword())) {
            String token = jwtUtil.generateToken(user);
            return token;
        } else if (instructor != null && encoder.matches(jwtRequest.getPassword(), instructor.getPassword())) {
            String token = jwtUtil.generateToken(instructor);
            return token;
        } else {
            return "Invalid Credentials";
        }
    }
}
