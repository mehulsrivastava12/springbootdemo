package com.example.demo.service;

import com.example.demo.DTO.JwtRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Service
public class MainService {

    @Autowired
    JwtService jwtService;

    public void logout(HttpServletResponse response){
        Cookie cookie = new Cookie("jwtToken", null);
        cookie.setMaxAge(0); // Set the cookie's expiration time to zero (0) to delete it
        cookie.setHttpOnly(true); // Make sure to use the same settings as when creating the cookie
        cookie.setSecure(true); // Set to true if you're using HTTPS

        response.addCookie(cookie);
    }

    public ResponseEntity<String> userLogin(String email, String password,HttpServletResponse response) {
        JwtRequest jwtRequest = new JwtRequest();
        jwtRequest.setPassword(password);
        jwtRequest.setUsername(email);
        response.setContentType("text/html");
        String token = jwtService.generateToken(jwtRequest);
        System.out.println("+++++++++++++++"+token);
        if (!token.equals("Invalid Credentials")) {
            setCookie(token,response);
            return ResponseEntity.ok("Login Successful");
        }
        else {
            return ResponseEntity.badRequest().body("Login Failed");
        }
    }

    public ResponseEntity<String> instructorLogin(String email, String password,HttpServletResponse response) {
        JwtRequest jwtRequest = new JwtRequest();
        jwtRequest.setPassword(password);
        jwtRequest.setUsername(email);
        response.setContentType("text/html");
        String token = jwtService.generateToken(jwtRequest);
        if (!token.equals("Invalid Credentials")) {
            setCookie(token,response);
            return ResponseEntity.ok("Login Successful");
        }
        else {
            return ResponseEntity.badRequest().body("Login Failed");
        }
    }

    public void setCookie(String token, HttpServletResponse response) {
//        token = "Bearer "+token;
        System.out.println("setCookie"+token);
        Cookie cookie = new Cookie("jwtToken", token);
        cookie.setMaxAge(30 * 60);
        cookie.setHttpOnly(true);
//        cookie.setSecure(true);
        response.addCookie(cookie);
    }
}
