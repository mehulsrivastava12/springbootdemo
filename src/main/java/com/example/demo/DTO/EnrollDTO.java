package com.example.demo.DTO;

import com.example.demo.entity.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor

//@SqlResultSetMapping(
//        name = "EnrollDTOMapping",
//        classes = @ConstructorResult(
//                targetClass = EnrollDTO.class,
//                columns = {
//                        @ColumnResult(name = "enrollmentId", type = Long.class),
//                        @ColumnResult(name = "title", type = String.class),
//                        @ColumnResult(name = "firstName", type = String.class),
//                        @ColumnResult(name = "lastName", type = String.class),
//                        @ColumnResult(name = "date", type = Date.class)
//                }
//        )
//)
public class EnrollDTO {

    private long enrollmentId;
    private String title;
    private String firstName;
    private String lastName;
    private Date date;
    private long courseId;

}
