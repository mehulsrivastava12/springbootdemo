package com.example.demo.DTO;

public enum OtpStatus {

    DELIVERED, FAILED
}
