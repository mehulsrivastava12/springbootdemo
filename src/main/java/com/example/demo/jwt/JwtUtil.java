package com.example.demo.jwt;

import com.example.demo.entity.Instructor;
import com.example.demo.entity.User;
import com.example.demo.repository.InstructorRepository;
import com.example.demo.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtUtil {

    private String SECRET_KEY = "Mehul@1234";
    public static final String BEARER_TOKEN_PREFIX = "Bearer ";

    @Autowired
    UserRepository userRepository;
    @Autowired
    InstructorRepository instructorRepository;

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public Claims extractAllClaims(String token) {
        token = token.replace(BEARER_TOKEN_PREFIX, "");
        Claims claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
        if (claims.getExpiration().after(new Date())) {
            return claims;
        }
        return null;
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("email", user.getEmail());
        return createToken(claims, user.getFirstName());
    }

    public String generateToken(Instructor instructor) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("email", instructor.getEmail());
        return createToken(claims, instructor.getFirstName());
    }

    private String createToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 300000))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token) {
        try {
            token = token.replace(BEARER_TOKEN_PREFIX, "");
            Claims claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
            if (claims.getExpiration().after(new Date())) {
                return true;
            }
        } catch (JwtException e) {
            e.printStackTrace();
        }
        return false;
    }


    public User extractUserDetailsFromToken(String token) {
        try {
            Claims claims = extractAllClaims(token);
            String email = claims.get("email").toString();
            User user = userRepository.findByEmail(email);

            // You can extract other user-related claims here if needed

            return user; // Replace with your UserDetails model
        } catch (Exception e) {
            return null;
        }
    }

    public Instructor extractInstructorDetailsFromToken(String token) {
        try {
            Claims claims = extractAllClaims(token);
            String email = claims.get("email").toString();
            Instructor instructor = instructorRepository.findByEmail(email);

            // You can extract other user-related claims here if needed

            return instructor; // Replace with your UserDetails model
        } catch (Exception e) {
            return null;
        }
    }
}
